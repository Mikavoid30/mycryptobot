export type TradeArgs = {
  price: number
  time: number
  size: number
}

export default class Trade {
  price: number
  time: number
  size: number

  constructor({ price, time, size }: TradeArgs) {
    this.price = price
    this.time = time
    this.size = size
  }
}
