import Trade from './trade'
import colors from 'colors'

export type PositionArgs = {
  trade: Trade
  id: string
}

export enum PositionStatus {
  CLOSED = 0,
  OPEN
}

export default class Position {
  status: PositionStatus
  enter: Trade
  exit: Trade | null
  id: string

  constructor({ trade, id }: PositionArgs) {
    this.status = PositionStatus.OPEN
    this.enter = trade
    this.exit = null
    this.id = id
  }

  close({ trade }: { trade: Trade }) {
    this.status = PositionStatus.CLOSED
    this.exit = trade
  }

  profit() {
    if (!this.exit) return 0
    const fee = 0.0025
    const entrance = this.enter.price * (1 + fee)
    const exit = this.exit.price * (1 - fee)
    return exit - entrance
  }

  print() {
    let profitStr = ''
    const enter = `Enter | ${this.enter.price} | ${this.enter.time}`
    const exit = this.exit
      ? `Exit | ${this.exit.price} | ${this.exit.time}`
      : colors.blue('STILL OPEN')

    if (this.status === PositionStatus.CLOSED) {
      let profit = this.profit()
      const colored =
        profit > 0
          ? colors.green(profit.toFixed(2))
          : colors.red(profit.toFixed(2))
      profitStr = `Profit : ${colored}`
    }
    console.log(`${enter} - ${exit} - ${profitStr}`)
  }
}
