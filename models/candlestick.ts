import moment from 'moment'

import { CandlestickArgs } from '../app/types/candlestickArgs'

export enum CandlestickStatus {
  CLOSED = 0,
  OPEN
}

export default class Candlestick {
  low: number
  high: number
  close: number
  open: number
  price: number
  interval: number
  startTime: Date
  volume: number
  status: CandlestickStatus

  constructor({
    low,
    high,
    close,
    open,
    price = 0,
    interval,
    startTime = moment().toDate(),
    volume
  }: CandlestickArgs) {
    this.low = low || price
    this.high = high || price
    this.close = close || price
    this.open = open || price
    this.price = price
    this.interval = interval
    this.startTime = startTime
    this.volume = volume || 1e-5
    this.status = CandlestickStatus[close ? 'CLOSED' : 'OPEN']
  }

  onPrice({ price, volume, time }: any) {
    if (this.status === CandlestickStatus.CLOSED) {
      throw new Error('Trying to add to closed candles')
    }
    this.price = price
    this.volume = volume

    if (this.high < price) this.high = price
    if (this.low > price) this.low = price

    this.close = price

    const delta = (time.getTime() - this.startTime.getTime()) * 1e-3
    if (delta >= this.interval / 100) {
      this.status = CandlestickStatus.CLOSED
    }
  }
  average() {
    return (this.close + this.high + this.low) / 3
  }
}
