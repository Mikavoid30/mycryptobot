import Coinbase from '../coinbase/coinbase'
import Strategy from '../strategy/strategy'
import strategyFactory from '../strategy/strategy.factory'
import TickerService from './TickerService'
import HistoricalService from './HistoricalService'
import colors from 'colors'
import Candlestick from '../../models/candlestick'
import { CandlestickStatus } from '../../models/candlestick'

import moment = require('moment')
import Position from '../../models/position'

const IS_SANDBOX = true

type TraderServiceArgs = {
  start: Date
  end: Date
  interval: number
  product: string
  strategyName: string
}

export default class TraderService {
  startTime: Date
  end: Date
  interval: number
  product: string
  coinbase: Coinbase
  strategy: Strategy
  historicalService: HistoricalService
  tickerService: TickerService
  currentCandle: Candlestick | null
  historyData: Candlestick[]

  constructor({
    start,
    end,
    interval,
    product,
    strategyName = 'macd'
  }: TraderServiceArgs) {
    this.historyData = []
    this.startTime = start
    this.end = end
    this.interval = interval
    this.product = product
    this.currentCandle = null
    this.strategy = strategyFactory({
      type: strategyName,
      opts: {
        onBuySignal: this.handleBuySignal.bind(this),
        onSellSignal: this.handleSellSignal.bind(this)
      }
    })

    const [baseCurrency, currency] = this.product.split('-')
    this.coinbase = new Coinbase(baseCurrency, currency, IS_SANDBOX)
    this.historicalService = new HistoricalService(this.coinbase, {
      start: this.startTime,
      end: this.end,
      interval: this.interval
    })
    this.tickerService = new TickerService(this.coinbase, {
      product: this.product,
      onTick: this.handleTick.bind(this),
      onError: this.handleError.bind(this)
    })
  }

  async start() {
    try {
      console.log('starting the trader service')
      this.historyData = await this.historicalService.getData()
      this.tickerService.start()
    } catch (error) {
      console.error(error)
    }
  }

  handleBuySignal(opts: any) {
    console.log('BUYING SIGNAL')
    this.strategy.positionOpened({
      price: opts.price,
      time: opts.time,
      amount: 4.0,
      id: '' + opts.time.getTime()
    })
  }

  handleSellSignal(opts: any) {
    console.log('SELLING SIGNAL')
    this.strategy.positionClosed({
      price: opts.price,
      time: opts.time,
      amount: 4.0,
      id: opts.position.id
    })
  }

  async handleTick(tick: any) {
    try {
      const time = moment(tick.time).toDate()
      console.log(
        colors.green(
          `Time: ${time.toDateString()}    Volume: ${
            tick.last_size
          }    Price: ${tick.price}`
        )
      )

      if (this.currentCandle) {
        this.currentCandle.onPrice({
          price: +tick.price,
          volume: +tick.last_size,
          time
        })
      } else {
        this.currentCandle = new Candlestick({
          price: +tick.price,
          high: +tick.price,
          low: +tick.price,
          open: +tick.price,
          interval: this.interval,
          startTime: time,
          volume: +tick.last_size
        })
      }

      const sticks = [...this.historyData]
      sticks.push(this.currentCandle)

      this.strategy.run({
        sticks,
        time
      })

      if (this.currentCandle.status === CandlestickStatus.CLOSED) {
        this.historyData.push(this.currentCandle)
        this.currentCandle = null
        this.printResults()
      }

      return Promise.resolve()
    } catch (e) {
      console.error(e)
    }
  }

  printResults() {
    const positions = Object.keys(this.strategy.positions).map(
      (k) => this.strategy.positions[k]
    )
    if (!positions.length) return
    const len = positions.length
    const lastPosition = positions[len - 1]
    lastPosition.print()
  }

  async handleError(error: any) {
    console.log('==> ERROR <==', error)
    return Promise.resolve()
  }
}
