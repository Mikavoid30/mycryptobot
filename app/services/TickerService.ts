import Coinbase from '../coinbase/coinbase'
import { WebsocketClient } from 'coinbase-pro'
import { Socket } from 'net'
import conf from '../../conf/conf'
export type TickerServiceArgs = {
  product: string
  onTick: (data: any) => any
  onError: (error: any) => any
}

export default class TickerService {
  running: boolean
  coinbase: any
  wsClient: WebsocketClient
  product: string
  onTick: (data: any) => any
  onError: (error: any) => any

  constructor(
    coinbase: Coinbase,
    { product, onTick, onError }: TickerServiceArgs
  ) {
    this.product = product
    this.onTick = onTick
    this.onError = onError
    this.coinbase = coinbase
    this.running = false
    this.wsClient = new WebsocketClient(
      [this.coinbase.currPair],
      conf(`COINBASE_PRO_API_URL_WSS`),
      null as any,
      { channels: ['ticker'] }
    )
  }

  start() {
    this.running = true
    this.wsClient.on('message', async (data: any) => {
      if (data.type === 'ticker') {
        this.onTick(data)
      }
    })

    this.wsClient.on('error', async (error: any) => {
      this.onError(error)
    })

    this.wsClient.on('close', () => {
      if (this.running) {
        this.wsClient.connect()
      }
    })
  }

  stop() {
    this.running = false
    this.wsClient.disconnect()
  }
}
