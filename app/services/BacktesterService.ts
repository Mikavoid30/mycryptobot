import { BacktesterServiceArgs } from '../types/backtesterServiceArgs'
import HistoricalService from './HistoricalService'
import Coinbase from '../coinbase/coinbase'
import Strategy from '../strategy/strategy'
import Candlestick from '../../models/candlestick'
import colors from 'colors'
import Position from '../../models/position'
import strategyFactory from '../strategy/strategy.factory'

const IS_SANDBOX = true

export default class BacktesterService {
  startTime: Date
  end: Date
  interval: number
  product: string
  coinbase: Coinbase
  historicalService: HistoricalService
  strategy: Strategy

  constructor({
    start,
    end,
    interval,
    product,
    strategyName = 'macd'
  }: BacktesterServiceArgs) {
    this.startTime = start
    this.end = end
    this.interval = interval
    this.product = product
    this.strategy = strategyFactory({
      type: strategyName,
      opts: {
        onBuySignal: this.handleBuySignal.bind(this),
        onSellSignal: this.handleSellSignal.bind(this)
      }
    })

    const [baseCurrency, currency] = this.product.split('-')
    this.coinbase = new Coinbase(baseCurrency, currency, IS_SANDBOX)
    this.historicalService = new HistoricalService(this.coinbase, {
      start: this.startTime,
      end: this.end,
      interval: this.interval
    })
  }

  async start() {
    try {
      const history = await this.historicalService.getData()
      const len = history.length

      console.log(
        colors.bold.yellow(
          '# Used strategy: ' + this.strategy.name() + ' on ' + this.product
        )
      )
      console.log(colors.green('From ' + this.startTime.toLocaleString()))
      console.log(colors.green('To ' + this.end.toLocaleString()))
      console.log(
        colors.bold.dim('# Number of candlesticks: ' + history.length)
      )

      for (let i = 1; i < len; i++) {
        const tick = history[i]
        const sticks: Candlestick[] = history.slice(0, 1 * i)
        const currenTime = tick.startTime
        await this.strategy.run({ sticks, time: currenTime })
      }

      const positions = Object.keys(this.strategy.positions).map(
        (k) => this.strategy.positions[k]
      )

      console.log(
        colors.bold.dim('# Number of positions taken:' + positions.length)
      )

      console.log(
        colors.bold.dim(
          '# Number of positions still opened:' +
            positions.filter((p) => p.status !== 0).length
        )
      )

      positions.forEach((position: Position) => {
        position.print()
      })
      const totalProfit = positions.reduce(
        (profit, current) => profit + current.profit(),
        0
      )
      console.log(colors.bold('#########################'))
      console.log(
        colors.bold.yellow('TOTAL PROFIT: '),
        totalProfit > 0
          ? colors.bold.green(totalProfit.toFixed(2))
          : colors.bold.red(totalProfit.toFixed(2))
      )
      console.log(colors.bold('#########################'))
    } catch (error) {
      console.log(error)
    }
  }

  handleBuySignal(opts: any) {
    // console.log('BUYING SIGNAL')
    this.strategy.positionOpened({
      price: opts.price,
      time: opts.time,
      amount: 4.0,
      id: '' + opts.time.getTime()
    })
  }

  handleSellSignal(opts: any) {
    // console.log('SELLING SIGNAL')
    this.strategy.positionClosed({
      price: opts.price,
      time: opts.time,
      amount: 4.0,
      id: opts.position.id
    })
  }
}
