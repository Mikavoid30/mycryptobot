import Coinbase from '../coinbase/coinbase'
import { HistoricalServiceOptions } from '../types/historicServiceOptions'
import moment from 'moment'
import Candlestick from '../../models/candlestick'

class HistoricalService {
  coinbase: Coinbase
  start: Date
  end: Date
  interval: number
  data: any[]

  constructor(
    coinbase: Coinbase,
    { start, end, interval }: HistoricalServiceOptions
  ) {
    this.coinbase = coinbase
    this.start = start
    this.end = end
    this.interval = interval || 300
    this.data = []
  }

  sleep(s: number) {
    return new Promise((resolve) => {
      setTimeout(resolve, s * 1000)
    })
  }
  async getData() {
    const requests = this.createRequests()

    for (let request of requests) {
      console.log('>>> Performing request')
      this.performRequest(request)
      await this.sleep(1 / 1.5)
    }

    // Reorder The data
    this.data = this.data.sort((a, b) => a[0] - b[0])
    const candlesticks = this.data.map((data) => {
      return new Candlestick({
        low: data[1],
        high: data[2],
        open: data[3],
        close: data[4],
        volume: data[5],
        interval: this.interval,
        startTime: moment.unix(data[0]).toDate()
      })
    })
    return candlesticks
  }

  async performRequest(request: any) {
    try {
      const data = await this.coinbase.getHistoricRates({
        start: request.start,
        end: request.end,
        interval: this.interval
      })
      this.data = [...this.data, ...data]
    } catch (e) {
      console.log('fucking error', e)
    }
  }

  createRequests() {
    const max = 300
    const delta = (this.end.getTime() - this.start.getTime()) * 1e-3
    const nbIntervals = delta / this.interval
    const numberRequests = Math.ceil(nbIntervals / max)

    const intervals = Array(numberRequests)
      .fill(null)
      .map((_, reqNum) => {
        return {
          request: reqNum,
          start: moment(this.start)
            .add(reqNum * max * this.interval, 'seconds')
            .toDate(),
          end: moment(this.start)
            .add((reqNum + 1) * max * this.interval, 'seconds')
            .toDate()
        }
      })
    return intervals
  }

  printPeriod(rates: any[] | null = this.data) {
    if (!rates) return console.log('No time frame')
    const length = rates.length
    const to = moment
      .unix(rates[0][0])
      .toDate()
      .toLocaleString()
    const from = moment
      .unix(rates[length - 1][0])
      .toDate()
      .toLocaleString()

    console.log(` Time frame from ${from} to ${to}`)

    return { from, to }
  }
}

export default HistoricalService
