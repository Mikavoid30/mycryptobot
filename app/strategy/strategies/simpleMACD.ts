import Strategy from '../strategy'
import { StrategyArgs } from '../../types/strategyArgs'
import Candlestick from '../../../models/candlestick'
import { indicators } from 'tulind'
import Position from '../../../models/position'

export default class SimpleMACDStrategy extends Strategy {
  constructor(opts: StrategyArgs) {
    super(opts)
  }

  name() {
    return 'SIMPLE MACD'
  }

  /**
   * Quand la MM rapide remonte et croise la lente en dessous su ZERO LINE - BUY
   * Quand la MM rapide recroise la mm lente au dessus de la ZERO LINE - SELL
   */
  async run({ sticks, time }: { sticks: Candlestick[]; time: Date }) {
    const sticksLen = sticks.length
    const prices: number[] = sticks.map((stick: Candlestick) => stick.average())
    const shortPeriod = 12
    const longPeriod = 26
    const signalPeriod = 9
    const indicator = indicators.macd.indicator

    const results = await indicator(
      [prices],
      [shortPeriod, longPeriod, signalPeriod]
    )

    const [macd, signal, histogram] = results
    const macdLen = macd.length
    const signalLen = signal.length
    const histogramLen = histogram.length

    if (histogramLen < 2 || macdLen < 2 || signalLen < 2) return

    const prevMacd = macd[macdLen - 2]
    const prevSignal = signal[signalLen - 2]
    const prevHisto = histogram[histogramLen - 2]

    const lastMacd = macd[macdLen - 1]
    const lastSignal = signal[signalLen - 1]
    const lastHisto = histogram[histogramLen - 1]

    const boundary = 0.2

    const wasAbove = prevHisto > boundary
    const wasBelow = prevHisto < -boundary
    const isAbove = lastHisto > boundary
    const isBelow = lastHisto < -boundary

    const openPositions = this.getOpenPositions()

    const price = sticks[sticksLen - 1].close

    if (wasAbove && isBelow) {
      if (openPositions.length === 0) {
        this.onBuySignal({
          price,
          amount: 1,
          time
        })
      }
    } else {
      openPositions.forEach((p: Position) => {
        if (wasBelow && isAbove) {
          if (p.enter.price * 1.005 < price || p.enter.price * 0.2 > price) {
            this.onSellSignal({
              price,
              size: p.enter.size,
              position: p,
              time
            })
          }
        } else {
          // if (p.enter.price * 0.95 > price) {
          //   this.onSellSignal({
          //     price,
          //     size: p.enter.size,
          //     position: p,
          //     time
          //   })
          // }
        }
      })
    }

    // if (prevHisto >= 0 && lastHisto < 0) {
    //   console.log('SHOULD SELL => ')
    //   console.log({ lastMacd, lastSignal, lastHisto })
    //   console.log({ prevMacd, prevSignal, prevHisto })
    // }
  }
}
