import Strategy from '../strategy'
import { StrategyArgs } from '../../types/strategyArgs'
import Candlestick from '../../../models/candlestick'
import Position from '../../../models/position'

export default class SimpleStrategy extends Strategy {
  constructor(opts: StrategyArgs) {
    super(opts)
  }

  name() {
    return 'SIMPLE'
  }

  async run({ sticks, time }: { sticks: Candlestick[]; time: Date }) {
    const len = sticks.length
    if (len < 20) {
      return
    }

    const penu = sticks[len - 2].close
    const last = sticks[len - 1].close

    const openPositions = this.getOpenPositions()

    if (openPositions.length === 0) {
      if (last < penu - penu * 0.005) {
        this.onBuySignal({ price: last, amount: 1, time })
      }
    } else {
      if (last > penu) {
        openPositions.forEach((p: Position) => {
          if (p.enter.price * 1.01 < last) {
            this.onSellSignal({
              price: last,
              size: p.enter.size,
              position: p,
              time
            })
          }
        })
      }
    }
  }
}
