import Candlestick from '../../models/candlestick'
import Position from '../../models/position'

export interface IStrategy {
  positionOpened: (position: any) => any
  positionClosed: (Position: any) => any
  getOpenPositions: () => Position[]
  name: () => string

  run: ({ sticks, time }: { sticks: Candlestick[]; time: Date }) => any
}
