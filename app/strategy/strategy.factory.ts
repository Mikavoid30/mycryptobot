import SimpleMACDStrategy from './strategies/simpleMACD'
import Simple from './strategies/simple'
import Strategy from './strategy'

export default function strategyFactory(data: any): Strategy {
  switch (data.type) {
    case 'simple':
      return new Simple(data.opts)
    default:
      // MACD is default
      return new SimpleMACDStrategy(data.opts)
  }
}
