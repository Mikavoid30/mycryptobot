import { StrategyArgs } from '../types/strategyArgs'
import { IStrategy } from './strategy.interface'
import Trade from '../../models/trade'
import Position from '../../models/position'
import Candlestick from '../../models/candlestick'
import { PositionStatus } from '../../models/position'

// export type Position = {
//   price: number
//   time: number
//   amount: number
//   order: number
// }

export default class Strategy implements IStrategy {
  onBuySignal: any
  onSellSignal: any
  positions: { [id: string]: Position }

  constructor({ onBuySignal, onSellSignal }: StrategyArgs) {
    this.onBuySignal = onBuySignal
    this.onSellSignal = onSellSignal
    this.positions = {}
  }

  async run({ sticks, time }: { sticks: Candlestick[]; time: Date }) {}

  getOpenPositions() {
    return Object.keys(this.positions)
      .map((p) => this.positions[p])
      .filter((p: Position) => p.status === PositionStatus.OPEN)
  }

  async positionOpened({ price, time, amount, id }: any) {
    const trade = new Trade({
      price,
      time,
      size: amount
    })

    const position = new Position({ trade, id })
    this.positions[id] = position
    return position
  }

  async positionClosed({ price, time, amount, id }: any) {
    const trade = new Trade({
      price,
      time,
      size: amount
    })

    const position = this.positions[id]
    if (position) {
      position.close({ trade })
    }
    return position
  }

  name() {
    return 'GENERAL STRATEGY'
  }
}
