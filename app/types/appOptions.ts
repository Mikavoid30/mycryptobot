export type AppOptions = {
  interval: number
  product: string
  start: Date
  end: Date
  strategyName: string
  isSandbox: boolean
}
