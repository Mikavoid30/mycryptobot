export type CandlestickArgs = {
  low: number
  high: number
  close?: number
  open: number
  price?: number
  interval: number
  startTime: Date
  volume: number
}
