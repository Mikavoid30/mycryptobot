import MongooseClient from '../database/clients/mongoose'

export type DBClient = MongooseClient | null
