export type BacktesterServiceArgs = {
  start: Date
  end: Date
  interval: number
  product: string
  strategyName: string
}
