export type HistoricalServiceOptions = {
  start: Date
  end: Date
  interval?: number
}
