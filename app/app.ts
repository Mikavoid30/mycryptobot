import program from 'commander'
import Coinbase from './coinbase/coinbase'
import MongooseClient from './database/clients/mongoose'
import Database from './database/database'
import { DBClient } from './types/dbclient.type'

import { AppOptions } from './types/appOptions'
import HistoricalService from './services/HistoricalService'
import moment from 'moment'
import BacktesterService from './services/BacktesterService'
import TickerService from './services/TickerService'
import TraderService from './services/TraderService'

class App {
  coinbase: Coinbase
  historicalService: HistoricalService
  db: Database
  conn: DBClient
  options: AppOptions

  constructor(dbClient: DBClient, options: AppOptions) {
    this.options = options
    const [baseCurrency, currency] = options.product.split('-')
    this.coinbase = new Coinbase(baseCurrency, currency, options.isSandbox)
    this.historicalService = new HistoricalService(this.coinbase, {
      start: options.start,
      end: options.end,
      interval: this.options.interval
    })
    this.db = new Database(dbClient)
    this.conn = null
  }

  async startBacktest() {
    console.log('>>> BACKTEST MODE STARTED <<<<')
    try {
      const backtestService = new BacktesterService({ ...this.options })
      backtestService.start()
    } catch (e) {
      console.log(e.message)
    }
  }

  async startLive() {
    // function handleTickerTick(data: any) {
    //   console.log('TICK', data)
    //   return null
    // }
    // function handleTickerError(error: any) {
    //   console.log('Error', error)
    //   return null
    // }
    console.log('>>> LIVE MODE STARTED <<<<')
    try {
      const traderService = new TraderService({ ...this.options })
      traderService.start()
      // const tickerService = new TickerService(this.coinbase, {
      //   product: this.options.product,
      //   onError: handleTickerError,
      //   onTick: handleTickerTick
      // })
      // tickerService.start()
    } catch (e) {
      console.log('BACKTEST MODE ERROR', e.message)
    }
  }
}

;(async function() {
  // Start the app
  const now = new Date().getTime()
  const yesterday = moment()
    .subtract(1, 'days')
    .toDate()
    .getTime()
  program
    .version('1.0.0')
    .option(
      '-i, --interval <seconds>',
      'Interval for candlesticks in seconds',
      '300'
    )
    .option('-p, --product <product>', 'Product identifier', 'BTC-EUR')
    .option(
      '-s, --start <start>',
      'Start Time ex: 2019-09-10 12:00:00',
      yesterday
    )
    .option('-e, --end <end>', 'End Time ex: 2019-09-10 12:00:00', now)
    .option('-l, --live', 'Flag to trade live')
    .option('-o, --prod', 'Flag for production mode')
    .option('-t, --strategy <strategy>', 'Name of the strategy', 'macd')
    .parse(process.argv)

  const app = new App(new MongooseClient(), {
    interval: +program.interval,
    product: program.product,
    start: moment(program.start).toDate(),
    end: moment(program.end).toDate(),
    strategyName: program.strategy,
    isSandbox: !program.prod
  })
  program.live ? app.startLive() : app.startBacktest()
})()
