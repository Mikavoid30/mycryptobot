import { DBClient } from '../types/dbclient.type'

export default class Database {
  db: any

  constructor(dbClient: DBClient) {
    if (!dbClient) throw new Error('No DB Client found')
    this.db = this.db ? this.db : dbClient.connect()
  }

  async getDb(): Promise<DBClient> {
    return await this.db
  }
}
