import mongoose from 'mongoose'
import conf from '../../../conf/conf'

interface IDBClient {
  connect: () => any
}

export default class MongooseClient implements IDBClient {
  connect(): Promise<any> {
    const mongoDbName = conf('MONGO_DATABASE_NAME')
    const mongoUser = conf('MONGO_USERNAME')
    const mongoPwd = conf('MONGO_PASSWORD')

    const uri = `mongodb+srv://${mongoUser}:${mongoPwd}@cluster0-vxbub.gcp.mongodb.net/${mongoDbName}?retryWrites=true&w=majority`
    return mongoose.connect(`${uri}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
  }
}
