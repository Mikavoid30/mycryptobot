import {
  PublicClient,
  AuthenticatedClient,
  WebsocketClient
} from 'coinbase-pro'
import conf from '../../conf/conf'
import colors from 'colors'

interface ICoinbase {
  client: any
  authedClient: any
  isSandbox: boolean
  getHistoricRates: (granularity?: number) => any
  getAccounts: () => any
  getWsClient: () => WebsocketClient
}

type CoinbaseEnv = {
  key: string
  secret: string
  passphrase: string
  url: string
  wsurl: string
}

export default class Coinbase implements ICoinbase {
  client: any
  wsClient: WebsocketClient | null
  authedClient: any
  baseCurr: string
  targetCurr: string
  currPair: string
  isSandbox: boolean
  env: CoinbaseEnv

  constructor(
    baseCurr: string = 'BTC',
    targetCurr: string = 'EUR',
    isSandbox: boolean
  ) {
    this.baseCurr = baseCurr
    this.targetCurr = targetCurr
    this.currPair = `${this.baseCurr}-${this.targetCurr}`
    this.isSandbox = isSandbox
    this.client = new PublicClient()
    this.wsClient = null

    this.env = this.getCredentials(isSandbox)
    this.authedClient = new AuthenticatedClient(
      this.env.key,
      this.env.secret,
      this.env.passphrase,
      this.env.url
    )
    isSandbox
      ? console.log(colors.bold.green('Running in SANDBOX MODE'))
      : console.log(colors.red.bold('Running in PRODUCTION MODE'))
  }

  getCredentials(sb: boolean): CoinbaseEnv {
    return {
      key: conf(`COINBASE_PRO_API_KEY${sb ? '_SBOX' : ''}`) || '',
      secret: conf(`COINBASE_PRO_API_SECRET${sb ? '_SBOX' : ''}`) || '',
      passphrase: conf(`COINBASE_PRO_API_PASSPHRASE${sb ? '_SBOX' : ''}`) || '',
      url: conf(`COINBASE_PRO_API_URL${sb ? '_SBOX' : ''}`) || '',
      wsurl: conf(`COINBASE_PRO_API_URL_WSS${sb ? '_SBOX' : ''}`) || ''
    }
  }

  getWsClient() {
    if (this.wsClient) return this.wsClient
    this.wsClient = new WebsocketClient(
      [this.currPair],
      this.env.wsurl,
      {
        key: this.env.key,
        secret: this.env.secret,
        passphrase: this.env.passphrase
      },
      { channels: ['ticker'] }
    )
    return this.wsClient
  }

  getHistoricRates({ start, end, interval = 300 }: any) {
    return this.client.getProductHistoricRates(this.currPair, {
      granularity: interval,
      start,
      end
    })
  }

  getAccounts() {
    return this.authedClient.getAccounts()
  }
}
